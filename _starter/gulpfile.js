const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const del = require('del');
const browserSync = require('browser-sync').create();
const webpackStream = require('webpack-stream');
const webpack = require('webpack');

const srcPath = {
  styles: './src/sass/**/*.sass',
  scripts: './src/js/main.js',
  images: './src/img/**/*',
  fonts: './src/fonts/**',
  html: './src/html/**/*'
};
const distPath = {
  styles: '../public/css',
  scripts: '../public/js',
  images: '../public/img',
  fonts: '../public/fonts',
  html: '../public/'
};
const vendor = {
  scripts : [
    'node_modules/jquery/dist/jquery.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/button.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
    // 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
  ]
};

const webpackConfig = {
  entry: {
    app: srcPath.scripts,
  },
  output: {
    path: __dirname + distPath.scripts,
    filename: 'main.js',
  },
  module: {
    rules: [
      {
        test: /.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
            ['env'],
          ],
        },
      },
    ],
  },
};

del([distPath.styles + '/**/*'], {force: true});
del([distPath.scripts + '/**/*'], {force: true});
del([distPath.images + '/**/*'], {force: true});


gulp.task('sass', () => {
  return gulp.src(srcPath.styles)
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'compressed'
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(distPath.styles))
    .pipe(browserSync.stream());
});

gulp.task('scripts', () => {
  return  gulp.src(srcPath.scripts)
    .pipe($.sourcemaps.init())
    .pipe(webpackStream(webpackConfig), webpack)
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(distPath.scripts))
});

gulp.task('vendors', () => {
  return gulp.src(vendor.scripts)
    .pipe($.concat('vendor.js'))
    .pipe($.babel({
      presets:['env']
    }))
    .pipe($.uglify())
    .pipe(gulp.dest(distPath.scripts))
});

gulp.task('styleLint', () => {
  return gulp.src(srcPath.styles)
    .pipe($.sassLint({
      options: {
        configFile: '.sass-lint.yml'
      }
    }))
    .pipe($.sassLint.format())
    .pipe($.sassLint.failOnError())
});

gulp.task('esLint', () => {
  return gulp.src(srcPath.scripts)
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());

});

gulp.task('imgOptimize', () => {
  return gulp.src(srcPath.images)
  //.pipe($.newer(distPath.images))
    .pipe($.imagemin())
    .pipe(gulp.dest(distPath.images))
});

gulp.task('fonts', () => {
  return gulp.src(srcPath.fonts)
    .pipe($.newer(distPath.fonts))
    .pipe(gulp.dest(distPath.fonts))
});

gulp.task('html', () => {
  return gulp.src(srcPath.html)
    .pipe($.htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(distPath.html))
});

gulp.task('serve', () =>{
  browserSync.init({
    proxy: 'cuda.local'
  });
});

gulp.task('watch', () => {
  gulp.watch(srcPath.styles, gulp.series('styleLint'));
  gulp.watch(srcPath.scripts, gulp.series('esLint'));
  gulp.watch(srcPath.styles, gulp.series(['sass']));
  gulp.watch(srcPath.scripts, gulp.series('scripts'));
  gulp.watch(srcPath.html, gulp.series('html'));
});

gulp.task('default',
  gulp.series(
    gulp.parallel(['styleLint','esLint']),
    gulp.parallel([ 'sass', 'scripts', 'vendors', 'imgOptimize', 'fonts']),
    gulp.parallel(['serve', 'watch'])
  ));
