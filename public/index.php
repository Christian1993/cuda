<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/main.css">
    <title>Cuda</title>
</head>
<body>

<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-sm-3"><img class="logo" src="./img/cuda_logo.png" alt="cuda logo"></div>
            <div class="col-sm-12 col-md-6 offset-md-0 offset-xl-3">
                <nav class="navbar">
                    <ul class="navbar__list">
                        <li class="navbar__list__item"><a class="tile tile--menu" href="#">home</a></li>
                        <li class="navbar__list__item"><a class="tile tile--menu" href="#">about</a></li>
                        <li class="navbar__list__item"><a class="tile tile--menu" href="#">work</a></li>
                        <li class="navbar__list__item"><a class="tile tile--menu" href="#">blog</a></li>
                        <li class="navbar__list__item"><a class="tile tile--menu" href="#">contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<section class="introduction">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="title-primary">Hi there! We are the new kids on the block<br class="d-none d-xl-block"> and we build awesome websites
                    and mobile apps.</h1>
                <a href="" class="button">Work with us!</a>
            </div>
        </div>
    </div>
</section>

<section class="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-secondary">services we provide</h2>
            </div>
            <div class="col-sm-8 offset-sm-2">
                <span class="line"></span>
                <p class="title-tertiary title-tertiary--services">We are working with both individuals and businesses
                    from all over the globe<br class="d-none d-xl-block">
                    to create awesome websites and applications.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="services__item">
                    <img src="./img/branding.png" alt="branding" class="services__img">
                    <p class="caption caption--services">branding</p>
                    <p class="description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                        nibh.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="services__item">
                    <img src="./img/design.png" alt="design" class="services__img">
                    <p class="caption caption--services">design</p>
                    <p class="description">Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="services__item">
                    <img src="./img/development.png" alt="development" class="services__img">
                    <p class="caption caption--services">development</p>
                    <p class="description">At vero eos et accusamus et iusto odio dignissimos qui blanditiis
                        praesentium.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="services__item">
                    <img src="./img/rocket.png" alt="rocket science" class="services__img">
                    <p class="caption caption--services">rocket science</p>
                    <p class="description">Et harum quidem rerum est et expedita distinctio. Nam libero tempore.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="team">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-secondary title-secondary--dark title-secondary--team">meet our beautiful team</h2>
            </div>
            <div class="col-sm-8 offset-sm-2">
                <span class="line line--gray"></span>
                <p class="title-tertiary title-tertiary--dark title-tertiary--team">We are a small team of designers and
                    developers, who help brands with big ideas.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="team__item">
                    <div class="team__img" style="background-image: url('./img/jason_statham.jpg')">
                    </div>
                    <p class="caption caption--dark caption--team">anne hathaway</p>
                    <p class="caption-secondary">CEO / Marketing Guru</p>
                    <p class="description description--dark">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                        sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
                    <div class="icon__wrapper">
                        <a href="#" class="icon__background"><i class="icon icon-facebook"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-twitter"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-linkedin"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-mail-alt"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="team__item">
                    <div class="team__img" style="background-image: url('./img/matt_damon.jpg')">
                        <img src="" alt="kate upton">
                    </div>
                    <p class="caption caption--dark caption--team">kate upton</p>
                    <p class="caption-secondary">Creative Director</p>
                    <p class="description description--dark">Duis aute irure dolor in in voluptate velit esse cillum
                        dolore fugiat nulla pariatur. Excepteur sint occaecat non diam proident.
                    </p>
                    <div class="icon__wrapper">
                        <a href="#" class="icon__background"><i class="icon icon-twitter"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-linkedin"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-mail-alt"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="team__item">
                    <div class="team__img" style="background-image: url('./img/liam_neeson.jpg')">
                        <img src="" alt="olivia wilde">
                    </div>
                    <p class="caption caption--dark caption--team">olivia wilde</p>
                    <p class="caption-secondary">Lead Designer</p>
                    <p class="description description--dark">Nemo enim ipsam voluptas sit aspernatur aut odit aut fugit,
                        sed quia consequuntur magni dolores eos qui ratione voluptatem nesciunt.
                    </p>
                    <div class="icon__wrapper">
                        <a href="#" class="icon__background"><i class="icon icon-facebook"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-twitter"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-linkedin"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-mail-alt"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="team__item">
                    <div class="team__img" style="background-image: url('./img/sylvester_stallone.jpg')">
                        <img src="" alt="ashley greene">
                    </div>
                    <p class="caption caption--dark caption--team">ashley greene</p>
                    <p class="caption-secondary">SEO / Developer</p>
                    <p class="description description--dark">Sed ut perspiciatis unde omnis iste natus error sit
                        voluptatem accusantium doloremque laudantium, totam rem aperiam.
                    </p>
                    <div class="icon__wrapper">
                        <a href="#" class="icon__background"><i class="icon icon-facebook"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-twitter"></i></a>
                        <a href="#" class="icon__background"><i class="icon icon-mail-alt"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="skills">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-secondary title-secondary--dark">we got skills</h2>
            </div>
            <div class="col-sm-8 offset-sm-2">
                <span class="line line--gray"></span>
                <p class="title-tertiary title-tertiary--dark title-tertiary--skills">Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit,
                    sed do eiusmod<br>
                    tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-6 col-xl-3">
                <div class="skills__item">
                    <div class="ring">
                        <div class="ring__percentage">
                            90<span class="ring__percentage-sign">%</span>
                        </div>
                    </div>
                    <div class="caption caption--dark caption--skills">web design</div>
                </div>
            </div>

            <div class="col-sm-6 col-xl-3">
                <div class="skills__item">
                    <div class="ring ring--pink">
                        <div class="ring__percentage">
                            75<span class="ring__percentage-sign">%</span>
                        </div>
                    </div>
                    <div class="caption caption--dark caption--skills">html / css</div>
                </div>
            </div>

            <div class="col-sm-6 col-xl-3">
                <div class="skills__item">
                    <div class="ring ring--green">
                        <div class="ring__percentage">
                            70<span class="ring__percentage-sign">%</span>
                        </div>
                    </div>
                    <div class="caption caption--dark caption--skills">graphic design</div>
                </div>
            </div>

            <div class="col-sm-6 col-xl-3">
                <div class="skills__item">
                    <div class="ring ring--orange">
                        <div class="ring__percentage">
                            85<span class="ring__percentage-sign">%</span>
                        </div>
                    </div>
                    <div class="caption caption--dark caption--skills">ui / ux</div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-secondary title-secondary--dark">our portfolio</h2>
            </div>
            <div class="col-sm-8 offset-sm-2">
                <span class="line line--yellow"></span>
                <p class="title-tertiary title-tertiary--dark title-tertiary--portfolio">Neque porro quisquam est, qui
                    dolorem ipsum quia dolor sit amet<br>
                    consectetur, adipisci velit, sed quia non numquam
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="portfolio__controls">
                    <ul class="portfolio__controls__list">
                        <li class="tile tile--portfolio" id="all">all</li>
                        <li class="tile tile--portfolio" id="web">web</li>
                        <li class="tile tile--portfolio" id="apps">apps</li>
                        <li class="tile tile--portfolio" id="icons">icons</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" id="portfolio-items">
            <div class="col-sm-6">
                <div class="portfolio__item">
                    <img class="portfolio__img" src="./img/isometric.png" alt="isometric mock-up">
                    <p class="caption--alt">isometric perspective mock-up</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="portfolio__item">
                    <img class="portfolio__img" src="./img/time_zone.png" alt="time zone ui">
                    <p class="caption--alt">time zone app ui</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="portfolio__item">
                    <img class="portfolio__img" src="./img/viro.png" alt="viro media">
                    <p class="caption--alt">viro media players ui</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="portfolio__item">
                    <img class="portfolio__img" src="./img/blog.png" alt="blog ui">
                    <p class="caption--alt">blog / magazine flat ui kit</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="#" class="button button--portfolio">Load more projects</a>
            </div>
        </div>
    </div>
</section>

<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-secondary title-secondary--dark">what people say about us</h2>
            </div>
            <div class="col-sm-8 offset-sm-2">
                <span class="line line--pink"></span>
                <p class="title-tertiary title-tertiary--dark title-tertiary--about">Our clients love us!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 about__wrapper">
                <div class="about__wrapper__item">
                    <div class="about__wrapper__img"></div>
                    <div class="about__wrapper__text">
                        <p class="sentence">“Nullam dapibus blandit orci, viverra gravida dui lobortis eget. Maecenas
                            fringilla urna eu nisl scelerisque.”</p>
                        <p class="caption caption--align-left">Chanel Iman</p>
                        <p class="caption-secondary caption-secondary--about">CEO of Pinterest</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 about__wrapper">
                <div class="about__wrapper__item">
                    <div class="about__wrapper__img"></div>
                    <div class="about__wrapper__text">
                        <p class="sentence">“Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In
                            condimentum facilisis porta.”</p>
                        <p class="caption caption--align-left">adriana lima</p>
                        <p class="caption-secondary caption-secondary--about">Founder of Instagram</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 about__wrapper">
                <div class="about__wrapper__item">
                    <div class="about__wrapper__img"></div>
                    <div class="about__wrapper__text">
                        <p class="sentence">“Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In
                            condimentum facilisis porta.”</p>
                        <p class="caption caption--align-left">anne hathaway</p>
                        <p class="caption-secondary caption-secondary--about">Lead Designer at Behance</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 about__wrapper">
                <div class="about__wrapper__item">
                    <div class="about__wrapper__img"></div>
                    <div class="about__wrapper__text">
                        <p class="sentence">“Phasellus non purus vel arcu tempor commodo. Fusce semper, purus vel luctus
                            molestie, risus sem cursus neque.”</p>
                        <p class="caption caption--align-left">emma stone</p>
                        <p class="caption-secondary caption-secondary--about">Co-Founder of Shazam</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="title-secondary">get in touch</h2>
            </div>
            <div class="col-sm-12">
                <span class="line line--blue"></span>
                <p class="title-tertiary title-tertiary--contact">1600 Pennsylvania Ave NW,
                    Washington, DC 20500, United States of America. Tel: (202) 456-1111</p>
            </div>
        </div>
        <form  class="form">
            <div class="row">
                <div class="col-sm-6">
                    <input class="form__input form__input--name" type="text" id="name" placeholder="Your Name *">
                </div>
                <div class="col-sm-6">
                    <input class="form__input form__input--email" type="text" id="email" placeholder="Your Email *">
                </div>
                <div class="col-sm-12">
                    <textarea class="form__input form__input--message" name="" id="message" cols="30" rows="10" placeholder="Your Message *"></textarea>
                </div>
                <div class="col-sm-12">
                    <button class="button button--contact" type="submit">send message</button>
                </div>
            </div>
        </form>
    </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="footer__wrapper">
                    <ul class="footer__list">
                        <li class="footer__item"><a class="link" href="#">Facebook</a></li>
                        <li class="footer__item"><a class="link" href="#">Twitter</a></li>
                        <li class="footer__item"><a class="link" href="#">Google+</a></li>
                        <li class="footer__item"><a class="link" href="#">Linkedin</a></li>
                        <li class="footer__item"><a class="link" href="#">Behance</a></li>
                        <li class="footer__item"><a class="link" href="#">Dribbble</a></li>
                        <li class="footer__item"><a class="link" href="#">GitHub</a></li>
                    </ul>
                </div>
            </div>
        </div>
</footer>

<!--<script src="./js/vendor.js"></script>-->
<!--<script src="./js/main.js"></script>-->
</body>
</html>