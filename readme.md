# PSD to Html / CSS project
### Description:
Fully responsive page made in Html and Sass. I've used BEM methodology combined with OOCSS, file structure is variation of 7-1 pattern.
In the project there is also imported bootstrap 4 grid via npm. 
Due to a little bit of inconsistent design I was forced to use many (too many in my opinion) modifiers.

### How to run
1. Download repo
2. Navigate to _starter
2. Type: _npm_ _install_
3. Type: _npm_ _run_ _dev_ or _gulp_


<img src="https://drive.google.com/uc?export=view&id=1ngxuwGHGFcCKOg-mTCssWx8nO85jvD04"  width="400" height="auto" align="left">
<img src="https://drive.google.com/uc?export=view&id=1M_CCQ0Igu0txViUqcMZNtBa7-6gOYSGv"  width="300" height="auto" align="left">
<img src="https://drive.google.com/uc?export=view&id=1Y0RpMyJz1v6gA3Wlri27JQGAeQa6S3OV"  width="150" height="auto" align="left">
